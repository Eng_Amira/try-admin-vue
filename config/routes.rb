Rails.application.routes.draw do


  resources :audit_logs
  resources :company_banks
  resources :admin_watchdogs
  resources :admin_logins
  resources :selfie_verifications,:except => [:new, :create]
  resources :address_verifications,:except => [:new, :create]
  resources :nationalid_verifications,:except => [:new, :create]
  resources :user_infos
  resources :user_verifications
  resources :user_verfications
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :watchdogs
      resources :users
      resources :countries
      resources :affilate_programs
      post "users/create" => "users#create"
      get "homepage" => "users#homepage"
      post "confirmmail" => "users#confirmmail"
      post "resend_confirmmail" => "users#resend_confirmmail"
      get "two_factor" => "users#get_two_factor"
      post "two_factor" => "users#post_two_factor"
      get "statistics" => "users#statistics"
      get "lock_unlock_account" => "users#lock_unlock_account"
      get "send_confirmation_email" => "users#send_confirmation_email"
      get "change_confirmation_code" => "users#change_confirmation_code"
      get "confirm_google_code" => "users#confirm_google_code"
      get "set_otp_code" => "users#set_otp_code"
      get "user_log" => "watchdogs#user_log"
      get "active_sessions" => "logins#active_sessions"
      post 'auth_user' => 'authentication#authenticate_user'
      post "unlockaccount" => "authentication#unlockaccount"
      post "send_invitation_email" => "affilate_programs#send_invitation_email"
      post 'getEditToken' => 'authentication#getEditToken'
      resources :cards_logs
      resources :cards_categories
      resources :cards
      get "search" => "cards#search"
      post "search" => "cards#searchpost"
      get "buy_card" => "cards#buy_card" 
      get "cards_status" => "cards#cards_status"
      get "display_cards_categories" => "cards_categories#display_cards_categories"
      resources :addresses
      get "search_address" => "addresses#search"
      post "search_address" => "addresses#searchpost"
      get "delete_user_addresses" => "addresses#delete_user_addresses"
      get "user_addresses" => "addresses#user_addresses"
      get "send_sms" => "user_wallets#send_sms"
      post "send_sms_post" => "user_wallets#send_sms_post"
      post "verifysms_post" => "user_wallets#verifysms_post"
      get "verifysms" => "user_wallets#verifysms"
      get "wallets_status" => "user_wallets#wallets_status"
      get "transfer_balance" => "user_wallets#transfer_balance"
      post "transfer_balance" => "user_wallets#transfer_balancepost"
      resources :wallets_transfer_ratios
      resources :user_wallets
      resources :sms_logs
      resources :notifications
      resources :notifications_settings
      get "user_notifications" => "notifications#user_notifications"
      get "search_notifications" => "notifications#search"
      get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
      resources :uploads
      resources :money_ops
      get "operations_status" => "money_ops#operations_status"
      resources :bank_accounts
      resources :ticket_departments
      resources :tickets
      get 'tickets/:id/replies' => 'tickets#showreplies'
      post 'tickets/reply' => 'tickets#create_reply'
      resources :withdraws

    end
  end

  resources :withdraws
  resources :admins,:except => [:new]
  resources :affilate_programs
  resources :countries
  resources :logins
  resources :verifications
  resources :watchdogs
  resources :users,:except => [:new]
  root "admins#homepage"
  get "confirmmail" => "users#confirmmail"
  get "unlockaccount" => "admins#unlockaccount"
  get "two_factor" => "admins#get_two_factor"
  post "two_factor" => "admins#post_two_factor"
  get "user_log" => "watchdogs#user_log"
  get "audit_log" => "audit_logs#user_audit_log"
  get "admin_log" => "admin_watchdogs#admin_log"
  get "statistics" => "admins#statistics"
  get "lock_unlock_account" => "users#lock_unlock_account"
  get "active_sessions" => "logins#active_sessions"
  get "admin_active_sessions" => "admin_logins#admin_active_sessions"
  get "send_confirmation_email" => "admins#send_confirmation_email"
  get "change_confirmation_code" => "admins#change_confirmation_code"
  get "confirm_google_code" => "admins#confirm_google_code"
  #get "affilate-program/:refered_by" => "affilate_programs#send_invitation"
  #post "send_invitation_email" => "affilate_programs#send_invitation_email"
  #get "edit_password/:id" => "users#edit_password" , :as => "edit_password" 
  get "add_admin" => "admins#add_admin"
  post "add_admin" => "admins#add_admin_post"
  get "error_page" => "logins#error_page"
  get "lock_page" => "admin_logins#lock_page"
  post "lock_page" => "admin_logins#check_to_unlock"
  resources :cards_logs
  resources :cards_categories
  resources :cards
  get "search" => "cards#search"
  post "search" => "cards#searchpost"
  get "buy_card" => "cards#buy_card" 
  get "cards_status" => "cards#cards_status"
  get "display_cards_categories" => "cards_categories#display_cards_categories"
  resources :addresses
  get "search_address" => "addresses#search"
  post "search_address" => "addresses#searchpost"
  get "delete_user_addresses" => "addresses#delete_user_addresses"
  get "user_addresses" => "addresses#user_addresses"
  get "send_sms" => "user_wallets#send_sms"
  post "send_sms_post" => "user_wallets#send_sms_post"
  post "verifysms_post" => "user_wallets#verifysms_post"
  get "verifysms" => "user_wallets#verifysms"
  get "wallets_status" => "user_wallets#wallets_status"
  get "user_list_data" => "user_wallets#user_list_data"
  get "transfer_balance" => "user_wallets#transfer_balance"
  post "transfer_balance" => "user_wallets#transfer_balancepost"
  get "admin_add_balance" => "user_wallets#admin_add_balance"
  post "admin_add_balance_post" => "user_wallets#admin_add_balance_post"
  resources :wallets_transfer_ratios
  resources :user_wallets
  resources :users_wallets_transfers
  resources :sms_logs
  resources :notifications
  resources :notifications_settings
  get "user_notifications" => "notifications#user_notifications"
  get "search_notifications" => "notifications#search"
  get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
  resources :uploads
  resources :money_ops
  get "operations_status" => "money_ops#operations_status"
  resources :bank_accounts
  resources :ticket_departments
  resources :tickets
  get 'tickets/:id/replies' => 'tickets#showreplies'
  post 'tickets/reply' => 'tickets#create_reply'
  get "user_bank_accounts" => "withdraws#user_bank_accounts"
  get "close_ticket" => "tickets#close_ticket"
  
  resources :passwords,
      controller: 'clearance/passwords',
      only: [:create, :new]

  resource :session,
      controller: 'clearance/sessions',
      only: [:create]

  resources :admins,
      controller: 'admins',
      only: Clearance.configuration.user_actions do
        resource :password,
          controller: 'clearance/passwords',
          only: [:create, :edit, :update]
      end

  get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
  delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'

  if Clearance.configuration.allow_sign_up?
   get '/sign_up' => 'admins#new', as: 'sign_up'
  end
  



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
