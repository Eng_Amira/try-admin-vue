class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :short_code
      t.string :Full_Name
      t.integer :Phone_code
      t.string :Currency
      t.string :language
      t.integer :active

      t.timestamps
    end
  end
end
