class CreateUserVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :user_verifications do |t|
      t.integer :user_id
      t.integer :user_info_id
      t.integer :document_type
      t.integer :status

      t.timestamps
    end
  end
end
