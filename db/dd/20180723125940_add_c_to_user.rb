class AddCToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :account_number, :integer
    add_column :users, :telephone, :string
    add_column :users, :account_currency, :boolean, default: false
  end
end
