require 'rails_helper'

RSpec.describe "ticket_replies/show", type: :view do
  before(:each) do
    @ticket_reply = assign(:ticket_reply, TicketReply.create!(
      :content => "Content"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Content/)
  end
end
