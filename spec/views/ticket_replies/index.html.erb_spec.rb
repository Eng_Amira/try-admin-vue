require 'rails_helper'

RSpec.describe "ticket_replies/index", type: :view do
  before(:each) do
    assign(:ticket_replies, [
      TicketReply.create!(
        :content => "Content"
      ),
      TicketReply.create!(
        :content => "Content"
      )
    ])
  end

  it "renders a list of ticket_replies" do
    render
    assert_select "tr>td", :text => "Content".to_s, :count => 2
  end
end
