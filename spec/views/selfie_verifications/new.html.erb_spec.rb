require 'rails_helper'

RSpec.describe "selfie_verifications/new", type: :view do
  before(:each) do
    assign(:selfie_verification, SelfieVerification.new(
      :user_id => 1,
      :note => "MyString"
    ))
  end

  it "renders new selfie_verification form" do
    render

    assert_select "form[action=?][method=?]", selfie_verifications_path, "post" do

      assert_select "input[name=?]", "selfie_verification[user_id]"

      assert_select "input[name=?]", "selfie_verification[note]"
    end
  end
end
