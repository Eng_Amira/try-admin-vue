require 'rails_helper'

RSpec.describe "admins/edit", type: :view do
  before(:each) do
    @admin = assign(:admin, Admin.create!(
      :username => "MyString",
      :firstname => "MyString",
      :lastname => "MyString"
    ))
  end

  it "renders the edit admin form" do
    render

    assert_select "form[action=?][method=?]", admin_path(@admin), "post" do

      assert_select "input[name=?]", "admin[username]"

      assert_select "input[name=?]", "admin[firstname]"

      assert_select "input[name=?]", "admin[lastname]"
    end
  end
end
