require 'rails_helper'

RSpec.describe "bank_accounts/show", type: :view do
  before(:each) do
    @bank_account = assign(:bank_account, BankAccount.create!(
      :bank_name => "MyText",
      :branche_name => "MyText",
      :swift_code => "MyText",
      :country => "MyText",
      :account_name => "MyText",
      :account_number => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
