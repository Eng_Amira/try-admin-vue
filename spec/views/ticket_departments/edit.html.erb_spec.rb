require 'rails_helper'

RSpec.describe "ticket_departments/edit", type: :view do
  before(:each) do
    @ticket_department = assign(:ticket_department, TicketDepartment.create!(
      :name => "MyString",
      :code => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit ticket_department form" do
    render

    assert_select "form[action=?][method=?]", ticket_department_path(@ticket_department), "post" do

      assert_select "input[name=?]", "ticket_department[name]"

      assert_select "input[name=?]", "ticket_department[code]"

      assert_select "input[name=?]", "ticket_department[description]"
    end
  end
end
