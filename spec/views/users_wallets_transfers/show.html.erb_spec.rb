require 'rails_helper'

RSpec.describe "users_wallets_transfers/show", type: :view do
  before(:each) do
    @users_wallets_transfer = assign(:users_wallets_transfer, UsersWalletsTransfer.create!(
      :user_id => 2,
      :user_to_id => 3,
      :transfer_method => 4,
      :transfer_type => 5,
      :hold_period => 6,
      :amount => 7.5,
      :ratio => 8.5,
      :approve => 9,
      :note => "Note"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7.5/)
    expect(rendered).to match(/8.5/)
    expect(rendered).to match(/9/)
    expect(rendered).to match(/Note/)
  end
end
