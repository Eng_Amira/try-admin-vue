require 'rails_helper'

RSpec.describe "company_banks/new", type: :view do
  before(:each) do
    assign(:company_bank, CompanyBank.new(
      :bank_key => "MyString",
      :bank_name => "MyString",
      :encryptkey => "MyString",
      :currency => "MyString",
      :country => "MyString",
      :city => "MyString",
      :branch => "MyString",
      :branch_code => "MyString",
      :phone => "MyString",
      :account_name => "MyString",
      :account_email => "MyString",
      :account_number => "MyString",
      :swiftcode => "MyString",
      :ibancode => "MyString",
      :visa_cvv => 1,
      :bank_category => "MyString",
      :bank_subcategory => "MyString",
      :fees => 1.5,
      :ratio => 1.5,
      :logo => "MyString",
      :expire_at => ""
    ))
  end

  it "renders new company_bank form" do
    render

    assert_select "form[action=?][method=?]", company_banks_path, "post" do

      assert_select "input[name=?]", "company_bank[bank_key]"

      assert_select "input[name=?]", "company_bank[bank_name]"

      assert_select "input[name=?]", "company_bank[encryptkey]"

      assert_select "input[name=?]", "company_bank[currency]"

      assert_select "input[name=?]", "company_bank[country]"

      assert_select "input[name=?]", "company_bank[city]"

      assert_select "input[name=?]", "company_bank[branch]"

      assert_select "input[name=?]", "company_bank[branch_code]"

      assert_select "input[name=?]", "company_bank[phone]"

      assert_select "input[name=?]", "company_bank[account_name]"

      assert_select "input[name=?]", "company_bank[account_email]"

      assert_select "input[name=?]", "company_bank[account_number]"

      assert_select "input[name=?]", "company_bank[swiftcode]"

      assert_select "input[name=?]", "company_bank[ibancode]"

      assert_select "input[name=?]", "company_bank[visa_cvv]"

      assert_select "input[name=?]", "company_bank[bank_category]"

      assert_select "input[name=?]", "company_bank[bank_subcategory]"

      assert_select "input[name=?]", "company_bank[fees]"

      assert_select "input[name=?]", "company_bank[ratio]"

      assert_select "input[name=?]", "company_bank[logo]"

      assert_select "input[name=?]", "company_bank[expire_at]"
    end
  end
end
