require "rails_helper"

RSpec.describe TicketDepartmentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/ticket_departments").to route_to("ticket_departments#index")
    end

    it "routes to #new" do
      expect(:get => "/ticket_departments/new").to route_to("ticket_departments#new")
    end

    it "routes to #show" do
      expect(:get => "/ticket_departments/1").to route_to("ticket_departments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/ticket_departments/1/edit").to route_to("ticket_departments#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/ticket_departments").to route_to("ticket_departments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/ticket_departments/1").to route_to("ticket_departments#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/ticket_departments/1").to route_to("ticket_departments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/ticket_departments/1").to route_to("ticket_departments#destroy", :id => "1")
    end
  end
end
