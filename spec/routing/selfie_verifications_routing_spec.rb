require "rails_helper"

RSpec.describe SelfieVerificationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/selfie_verifications").to route_to("selfie_verifications#index")
    end

    it "routes to #new" do
      expect(:get => "/selfie_verifications/new").to route_to("selfie_verifications#new")
    end

    it "routes to #show" do
      expect(:get => "/selfie_verifications/1").to route_to("selfie_verifications#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/selfie_verifications/1/edit").to route_to("selfie_verifications#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/selfie_verifications").to route_to("selfie_verifications#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/selfie_verifications/1").to route_to("selfie_verifications#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/selfie_verifications/1").to route_to("selfie_verifications#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/selfie_verifications/1").to route_to("selfie_verifications#destroy", :id => "1")
    end
  end
end
