require "rails_helper"

RSpec.describe TicketRepliesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/ticket_replies").to route_to("ticket_replies#index")
    end

    it "routes to #new" do
      expect(:get => "/ticket_replies/new").to route_to("ticket_replies#new")
    end

    it "routes to #show" do
      expect(:get => "/ticket_replies/1").to route_to("ticket_replies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/ticket_replies/1/edit").to route_to("ticket_replies#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/ticket_replies").to route_to("ticket_replies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/ticket_replies/1").to route_to("ticket_replies#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/ticket_replies/1").to route_to("ticket_replies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/ticket_replies/1").to route_to("ticket_replies#destroy", :id => "1")
    end
  end
end
