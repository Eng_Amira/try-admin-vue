require "rails_helper"

RSpec.describe AdminWatchdogsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/admin_watchdogs").to route_to("admin_watchdogs#index")
    end

    it "routes to #new" do
      expect(:get => "/admin_watchdogs/new").to route_to("admin_watchdogs#new")
    end

    it "routes to #show" do
      expect(:get => "/admin_watchdogs/1").to route_to("admin_watchdogs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin_watchdogs/1/edit").to route_to("admin_watchdogs#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/admin_watchdogs").to route_to("admin_watchdogs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin_watchdogs/1").to route_to("admin_watchdogs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin_watchdogs/1").to route_to("admin_watchdogs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin_watchdogs/1").to route_to("admin_watchdogs#destroy", :id => "1")
    end
  end
end
