require 'rails_helper'

RSpec.describe 'Countries API', type: :request do
  # initialize test data 
  let!(:countries) { create_list(:country, 1) }
  let(:country_id) { countries.first.id }
  
   # Create test user
   let(:user) { User.create(username: 'Amira' ,password: 'Amira123456', email: 'amira@yahoo.com', status: '1', roleid: '1', active_otp: '1' , country_id: country_id) }
   # authorize request
   #let(:headers) { { 'Authorization' => token_generator(user.id),"Content-Type" => "application/json" } }
   #let(:headers) { { 'Authorization' => JsonWebToken.encode(user_id: user.id),"Content-Type" => "application/json" } }
   let(:headers) { valid_headers }

  # Test suite for GET /countries
  describe 'GET /api/v1/countries' do
    # make HTTP get request before each example
    before { get '/api/v1/countries' }

    it 'returns countries' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(1)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /api/v1/countries/:id
  describe 'GET /api/v1/countries/:id' do
    before { get "/api/v1/countries/#{country_id}" }

    context 'when the record exists' do
      it 'returns the country' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(country_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:country_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Country/)
      end
    end
  end



  # Test suite for POST /countries
  describe 'POST /api/v1/countries' do
    # valid payload
    let(:valid_attributes) { { short_code: 'EGP', Phone_code: '057', Full_Name: 'EGYPT', Currency: 'EGP', language: 'Arabic', active: '1'} }
    
    context 'when the request is valid' do
     # let(:country) { Country.create(short_code: 'EGP', Phone_code: '057', Full_Name: 'EGYPT', Currency: 'EGP', language: 'Arabic', active: '1') }

    #it "returns http success" do
    #  expect(country.short_code).to eq('EGP')
    #end
    
      before { post '/api/v1/countries', params: {country: valid_attributes}, headers: headers}
      
      it 'creates a country' do
        expect(json['short_code']).to eq('EGP')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/countries', params: {country: {short_code: 'SA'} } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
        .to match("{\"Phone_code\":[\"can't be blank\",\"is not a number\"],\"Full_Name\":[\"can't be blank\"],\"Currency\":[\"can't be blank\"],\"language\":[\"can't be blank\"],\"active\":[\"can't be blank\",\"is not included in the list\"]}")
        #to match("{\"errors\":[\"Not Authenticated\"]}")   
      end
    end
  end


  # Test suite for PUT /api/v1/countries/:id
  describe 'PUT /api/v1/countries/:id' do
    let(:valid_attributes) { { short_code: 'EGP' } }

    context 'when the record exists' do
      before { put "/api/v1/countries/#{country_id}", params: {country: valid_attributes} }

      it 'updates the record' do
        expect(response.body).not_to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  
  # Test suite for DELETE /countries/:id
  describe 'DELETE /countries/:id' do
    before { delete "/api/v1/countries/#{country_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
 
end