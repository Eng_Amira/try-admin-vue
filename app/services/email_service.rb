# app/services/email_service.rb

class EmailService

    attr_accessor :user_mail, :subject , :text , :via , :email_provider
    require 'mailgun'

    # GET Email params
    # @param [Integer] user_mail The receiver user email address.
    # @param [String] subject The subject of the email.
    # @param [String] text The email content text.
    # @param [String] via the sender email address(payers@payers.com).
    # @param [String] EmailService_Provider The Email provider name.
    def initialize(user_mail:'client@payers.com',subject:'Welcome To Payers',text:'Welcome',via:'payers@payers.com',email_provider:'mailgun')
      
      @user_mail = user_mail
      @subject = subject
      @text = text
      @via = via
      @Email_Provider = email_provider

    end

    # Call a specific Email Provider and a specific function accourding to configuration
    def call

      if @Email_Provider == 'mailgun'

        if @subject == 'wallet transfer balance' || @subject == 'Recharge Wallet Balance' || @subject == 'Balance Withdraw' || @subject == 'Tickets Status Updates' || @subject == 'Tickets Replies'
          EmailMailgun.new(user_mail:@user_mail,subject:@subject,text:@text,via:@via).send_email
        elsif @subject == 'confirm sign in'
          EmailMailgun.new(user_mail:@user_mail,text:@text).confirmsignin
        elsif @subject == 'confirm mail'
          EmailMailgun.new(user_mail:@user_mail,text:@text).confirmmail
        end

      end

    end

end