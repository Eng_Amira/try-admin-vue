class TicketReply < ApplicationRecord
    belongs_to :ticket
    validates_presence_of :content, :message => "the content can't be empty"
    enum sender_type: { "Payers Support": 0, User: 1 }

    def self.create_reply
        @tickets = Ticket.where(responded: 0).all
        @tickets.each do |ticket|
        TicketReply.create(:content => 'مرحبا  في بايرز شكرا للتذكره جاري التواصل مع  القسم المختص', :ticket_id => ticket.id)
        Ticket.where(id: ticket.id).update_all(responded: 1)
        end
    end
end