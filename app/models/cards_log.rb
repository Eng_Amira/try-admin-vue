class CardsLog < ApplicationRecord

    validates :user_id, numericality: { only_integer: true , greater_than: 0 }
    validates :action_type, numericality: { only_integer: true , greater_than: 0 }
    validates :card_id, numericality: { only_integer: true , greater_than: 0 }
    belongs_to :card , foreign_key: "card_id"
    belongs_to :user , foreign_key: "user_id"
end
