class MoneyOp < ApplicationRecord
    validates_uniqueness_of :opid
    before_save :default_values
    belongs_to :user ,:foreign_key => "user_id"

    # Add default value to opid before save
    def default_values
        self.opid ||= "PY"+[*('A'..'Z'),*('0'..'9')].to_a.shuffle[0,10].join
    end
end
