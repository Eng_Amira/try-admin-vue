class BankAccount < ApplicationRecord
    validate :check_length
    belongs_to :user ,:foreign_key => "user_id"
    has_many :withdraw , foreign_key: "bank_id"
    def check_length
        unless swift_code.size == 8 or swift_code.size == 11
        errors.add(:swift_code, "length must be 8 or 11") 
        end
    end
    validates :country, length: { is: 3 }
    validates :swift_code, format: { with: /\A[a-zA-Z0-9]+\z/ }
end
