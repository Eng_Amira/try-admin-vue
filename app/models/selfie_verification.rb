class SelfieVerification < ApplicationRecord
    has_one_attached :avatar
    has_one :user_info , foreign_key: "selfie_verification_id"
    belongs_to :user ,:foreign_key => "user_id"
    enum status: { UnVerified: 0, Verified: 1 }

    validate :attachment_type
    validates_format_of :note, :with => /\A[^`!@#\$%\^&*+_=<>]+\z/, if: :validate_note?

    def attachment_type
        if avatar.attached? && !avatar.attachment.blob.content_type.in?(%w(image/png image/jpeg))
            errors.add(:avatar, 'Must be an image file or extension is not included in the list')
        elsif !avatar.attached?
            errors.add(:attachment, 'attachment is required')
        end
        
        #if avatar.attached? && !avatar.content_type.in?(%w(application/msword application/pdf))
         # errors.add(:avatar, 'Must be a PDF or a DOC file')
        #end
    end

    def validate_note?
        note.present?
    end
end
