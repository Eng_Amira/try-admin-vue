class UserWallet < ApplicationRecord
    validates_uniqueness_of :uuid
    validates_uniqueness_of :user_id
    before_save :default_values
    belongs_to :user , foreign_key: "user_id"
    

    def default_values
        self.currency ||= "USD"
        self.uuid ||= SecureRandom.hex(6)
    end

    # Check before confirming transfer balance between users
    # @param [Integer] userfrom The sender user ID.
    # @param [Integer] userto The receiver user ID.
    # @param [Float] transferamount The transferred amount between two users.
    # @return [message] error message if there is any error occurs while checking process.
    def self.checktransfer(user_from ,user_to ,transfer_amount,hold_period)

        @user_from_exist = UserWallet.where(user_id: user_from.to_i).first
        @user_to_exist = UserWallet.where(user_id: user_to.to_i).first
        @max_transfer = WalletsTransferRatio.where(key: 'max_transfer').pluck(:value).first
        @min_transfer = WalletsTransferRatio.where(key: 'min_transfer').pluck(:value).first
        

        @message = ""

        if @user_from_exist != nil
            if @user_from_exist.amount.to_f < transfer_amount.to_f
                @message  = @message + "You don't have enough money for this operation"
            end

            if transfer_amount.to_f < @min_transfer.to_f
                @message  = @message + "Sorry, The transferd balance must be greater than #{@min_transfer} USD"
            end

            if transfer_amount.to_f > @max_transfer.to_f
                @message  = @message + "Sorry, The transferd balance must be less or equal than #{@max_transfer} USD"
            end

            if @user_from_exist.status == 0
                @message  = @message + "Sorry, Your Wallet was Disabled"
            end

            if hold_period.to_i < 0
                @message  = @message + "Sorry, The holding period can't be less than zero"
            end
        else
            @message  = @message + "The sender User Wasn't Stored in Our Database"
        end

        if @user_to_exist == nil
            @message  = @message + "The receiver User Wasn't Stored in Our Database"
        else
            if @user_to_exist.status == 0
                @message  = @message + "Sorry, The Receiver User Wallet was Disabled"
            end
        end      

        if user_from == user_to
            @message  = @message + "You can't transfer balance to yourself"
        end

        return   @message 
    end
end
