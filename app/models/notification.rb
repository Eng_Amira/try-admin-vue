class Notification < ApplicationRecord
    validates :user_id, presence: true
    validates :notification_type, presence: true
    validates :title, presence: true
    validates :description, presence: true
    belongs_to :user ,:foreign_key => "user_id"
end
