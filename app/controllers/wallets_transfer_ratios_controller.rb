class WalletsTransferRatiosController < ApplicationController
  before_action :set_wallets_transfer_ratio, only: [:edit, :update]

  def index
    @wallets_transfer_ratios = WalletsTransferRatio.all
  end

  def new
    @wallets_transfer_ratio = WalletsTransferRatio.new
  end

  def edit
  end

  def create
    @wallets_transfer_ratio = WalletsTransferRatio.new(wallets_transfer_ratio_params)

    respond_to do |format|
      if @wallets_transfer_ratio.save
        format.html { redirect_to wallets_transfer_ratios_path, notice: 'Wallets transfer ratio was successfully created.' }
        format.json { render :show, status: :created, location: @wallets_transfer_ratio }
      else
        format.html { render :new }
        format.json { render json: @wallets_transfer_ratio.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @wallets_transfer_ratio.update(wallets_transfer_ratio_params)
        format.html { redirect_to wallets_transfer_ratios_path, notice: 'Wallets transfer ratio was successfully updated.' }
        format.json { render :show, status: :ok, location: @wallets_transfer_ratio }
      else
        format.html { render :edit }
        format.json { render json: @wallets_transfer_ratio.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wallets_transfer_ratio
      @wallets_transfer_ratio = WalletsTransferRatio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wallets_transfer_ratio_params
      params.require(:wallets_transfer_ratio).permit(:key, :value, :description)
    end
end
