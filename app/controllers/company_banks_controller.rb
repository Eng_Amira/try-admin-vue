class CompanyBanksController < ApplicationController
  before_action :set_company_bank, only: [:show, :edit, :update, :destroy]

  # Get list of all the user company banks
  # @return [id] company bank unique ID (Created automatically).
  # @return [bank_key] bank key name.
  # @return [bank_name] bank name.
  # @return [encryptkey] bank encrypted key.
  # @return [currency] bank currency.
  # @return [country] bank country.
  # @return [city] bank city.
  # @return [branch] bank branch name.
  # @return [branch_code] bank branch code.
  # @return [phone] bank phone number.
  # @return [account_name] company account name.
  # @return [account_email] company account email.
  # @return [account_number] company account number.
  # @return [swiftcode] bank swift code.
  # @return [ibancode] bank iban code(unique name).
  # @return [bank_category] bank category(local_bank,electronic_bank,digital_bank) .
  # @return [bank_subcategory] bank subcategory(local_eg , local_sa).
  # @return [fees] company bank fees.
  # @return [ratio] bank ratio.
  # @return [expire_at] company bank account expiration date.
  # @return [status] company bank account status (0 for disabled, 1 for enabled).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @company_banks = CompanyBank.all
  end

  # GET a spacific company bank and display it
  # @param [Integer] id company bank unique ID (Created automatically).
  # @return [bank_key] bank key name.
  # @return [bank_name] bank name.
  # @return [encryptkey] bank encrypted key.
  # @return [currency] bank currency.
  # @return [country] bank country.
  # @return [city] bank city.
  # @return [branch] bank branch name.
  # @return [branch_code] bank branch code.
  # @return [phone] bank phone number.
  # @return [account_name] company account name.
  # @return [account_email] company account email.
  # @return [account_number] company account number.
  # @return [swiftcode] bank swift code.
  # @return [ibancode] bank iban code(unique name).
  # @return [bank_category] bank category(local_bank,electronic_bank,digital_bank) .
  # @return [bank_subcategory] bank subcategory(local_eg , local_sa).
  # @return [fees] company bank fees.
  # @return [ratio] bank ratio.
  # @return [expire_at] company bank account expiration date.
  # @return [status] company bank account status (0 for disabled, 1 for enabled).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET /company_banks/new
  def new
    @company_bank = CompanyBank.new
  end

  # GET /company_banks/1/edit
  def edit
  end

  # POST /company_banks
  # POST /company_banks.json
  def create
    @company_bank = CompanyBank.new(company_bank_params)

    respond_to do |format|
      if @company_bank.save
        format.html { redirect_to @company_bank, notice: 'Company bank was successfully created.' }
        format.json { render :show, status: :created, location: @company_bank }
      else
        format.html { render :new }
        format.json { render json: @company_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_banks/1
  # PATCH/PUT /company_banks/1.json
  def update
    respond_to do |format|
      if @company_bank.update(company_bank_params)
        format.html { redirect_to @company_bank, notice: 'Company bank was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_bank }
      else
        format.html { render :edit }
        format.json { render json: @company_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_banks/1
  # DELETE /company_banks/1.json
  def destroy
    @company_bank.destroy
    respond_to do |format|
      format.html { redirect_to company_banks_url, notice: 'Company bank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_bank
      @company_bank = CompanyBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_bank_params
      params.require(:company_bank).permit(:bank_key, :bank_name, :encryptkey, :currency, :country, :city, :branch, :branch_code, :phone, :account_name, :account_email, :account_number, :swiftcode, :ibancode, :visa_cvv, :bank_category, :bank_subcategory, :fees, :ratio, :logo, :expire_at, :status)
    end
end
