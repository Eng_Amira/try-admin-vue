class UserWalletsController < ApplicationController
  before_action :set_user_wallet, only: [:show]

  # GET list of Users Wallets and display it
  # @return [id] Users Wallets unique ID (Created automatically).
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters ).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @user_wallets = UserWallet.all
  end

  # GET a spacific User Wallet and display it
  # @param [Integer] id User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new User Wallet
  # @param [Integer] id User Wallet unique ID (Created automatically).
  # @param [String] currency Users Wallets Currency (default = USD).
  # @param [Float] amount User Wallet amount.
  # @param [Integer] user_id Wallet User ID.
  # @param [Integer] status User Wallet status (0 for Disabled & 1 for Enabled).
  # @param [String] uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  def new
    @user_wallet = UserWallet.new
  end

  # GET a new pin code SMS
  # @param [Integer] tel User phone number preceded by country code.
  def send_sms
  end

  # POST a new pin code SMS and send it to user
  # @return [pinId] SMS return pinId that was coming from the SMS provider.
  def send_sms_post
    SMSJob.perform_async(tel:params[:tel])
    redirect_to(verifysms_path)
  end

  # GET verify SMS code
  # @param [Integer] code SMS code that was sent to the user.
  def verifysms
  end

  # POST verify SMS code
  # @return [verified] Code Verification status(ture , false).
  def verifysms_post

    @code = params[:code].to_i
    @applications_list = SMSService.new(code:@code).call

    if @applications_list["verified"] ==  true
      redirect_to sms_logs_path, notice: 'Valid code, successfully activated'  
    else 
      redirect_to sms_logs_path, notice: 'Wrong code Try again later'
    end

  end

  # POST a new User Wallet and save it
  # @return [id] User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    #WalletCreator.call(params[:currency])
    # @user_wallet = UserWallet.new(user_wallet_params)
    # @user_wallet.currency = "USD"
    # @user_wallet.uuid = SecureRandom.hex(6)

    respond_to do |format|
      @user_wallet = UserWallet.new(user_wallet_params)
      if @user_wallet.save
        format.html { redirect_to @user_wallet, notice: 'User wallet was successfully created.' }
        format.json { render :show, status: :created, location: @user_wallet }
      else
        format.html { render :new }
        format.json { render json: @user_wallet.errors, status: :unprocessable_entity }
      end
    end

  end

  # Change User Wallet status
  # @param [Integer] id User Wallet unique ID (Created automatically).
  # @param [Integer] status User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [id] User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def wallets_status

    @wallets_status = UserWallet.where(:id => params[:id].to_i).first
    if @wallets_status.status ==  0
      @wallets_status.update(:status => 1)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Enabled')
    elsif @wallets_status.status ==  1
      @wallets_status.update(:status => 0)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Disabled')
    else
      redirect_to(user_wallets_path,:notice => 'Sorry, something went wrong')
    end

  end

  # Transfer Balances between users wallets
  # @param [Integer] userfrom The sender user ID.
  # @param [Integer] userto The receiver user ID.
  # @param [Float] transferamount The transferred amount between two users.
  def transfer_balance   
  end

  # Post a transferred balance between two users
  # @return [userfrom] The sender user ID.
  # @return [userto] The receiver user ID.
  # @return [transferamount] The transferred amount between two users.
  # @return [new_balance_from] The sender new balance after transfer.
  # @return [new_balance_to] The receiver new balance after transfer.
  def transfer_balancepost
    @user_from = params[:userfrom]
    @user_to = params[:userto]
    @transfer_amount = params[:transferamount]
    @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount)
    if @message == ""
      ActiveRecord::Base.transaction do
      @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
      @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
      @user_data_from = User.where("id = ? ", @user_from).first
      @user_data_to = User.where("id = ? ", @user_to).first
      @new_balance_from = @user_wallet_from.amount.to_f - @transfer_amount.to_f
      @new_balance_to = @user_wallet_to.amount.to_f + @transfer_amount.to_f
      @user_wallet_from.update(:amount => @new_balance_from )
      @user_wallet_to.update(:amount => @new_balance_to )
      @smstext = "#{@transfer_amount} USD has been successfully transferred to user #{@user_data_to.firstname} #{@user_data_to.lastname} by payers admin"
      @tel = @user_data_from.telephone
      @user_mail = @user_data_from.email
      @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
      if @user_notification_setting.money_transactions != 0
         Notification.create(user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions)
      end
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@tel,@smstext)
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@tel,@smstext)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      end
      
      redirect_to(user_wallets_path,:notice => "Balance was successfully transferred")
      end
    else
      redirect_to(transfer_balance_path,:notice => @message )
    end
  end

  def admin_add_balance  
  end

  def admin_add_balance_post
    @receiver_user = params[:receiver_user].to_i
    @balance_amount = params[:balance_amount]
    @user_data = User.where("id = ?", @receiver_user).first   if  @receiver_user.present?
    @user_wallet = UserWallet.where("user_id = ?", @receiver_user).first if  @user_data.present?
    if @user_wallet != nil
      ActiveRecord::Base.transaction do
        @user_new_balance = @user_wallet.amount + @balance_amount.to_f
        @user_wallet.update(:amount => @user_new_balance)
        @smstext = "#{@balance_amount} USD has been successfully added to your wallet by payers admin"
        @user_notification_setting = NotificationsSetting.where(user_id: @user_data.id).first
        if @user_notification_setting.money_transactions != 0
          Notification.create(user_id: @user_data.id ,title: "Admin Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions)
        end
        if @user_notification_setting.money_transactions == 3
          SMSNotification.sms_notification_setting(@user_data.telephone,@smstext)
          SmsLog.create(:user_id => @user_data.id, :pinid => @smstext,:status => 1,:sms_type => 3)
          EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:'wallet transfer balance',text:@smstext)
        elsif @user_notification_setting.money_transactions == 2
          SMSNotification.sms_notification_setting(@user_data.telephone,@smstext)
          SmsLog.create(:user_id => @user_data.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        elsif @user_notification_setting.money_transactions == 1
          EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:'wallet transfer balance',text:@smstext)
        end
        redirect_to(user_wallets_path,:notice => "Balance was successfully Added")
      end
    else
      redirect_to(admin_add_balance_path,:notice => "Sorry this user wasn't stored in our database")
    end
  end

  def user_list_data
    @user_id = params[:user_id]
    @userdata = User.where("email LIKE ? OR account_number LIKE ?","%#{@user_id}%","%#{@user_id}%").pluck('firstname', 'lastname' , 'id') if  @user_id.present?
    render :json => {'userdata': @userdata }
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_wallet
      @user_wallet = UserWallet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_wallet_params
      params.require(:user_wallet).permit(:currency, :amount, :user_id, :status, :uuid)
    end
end
