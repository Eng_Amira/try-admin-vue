class LoginsController < ApplicationController
  before_action :require_login
  before_action :set_login, only: [:show, :edit, :update, :destroy]

  # show list of users logs
  # works only if current user is an admin
  # @return [Integer] user_id
  # @return [String] ip_address
  # @return [String] user_agent	
  # @return [String] device_id
  def index
    if current_user.roleid == 1
      @logins = Login.all
      else
        redirect_to root_path , notice: "not allowed" 
    end   
  end
  
  # show user's active sessions
  # works only if current user is an admin
  # @param [Integer] user_id
  def active_sessions
    if current_user.roleid == 1 
      @logins = Login.where("user_id =? ",params[:id])
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # show details of a log
  # @param [Integer] id
  # @return [Integer] user_id
  # @return [String] ip_address
  # @return [String] user_agent	
  # @return [String] device_id
  # @return [String] operation_type	
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /logins/new
  def new
    if current_user.roleid == 1 
      @login = Login.new
    else
      redirect_to root_path , notice: "not allowed" 
    end
    
  end

  # GET /logins/1/edit
  def edit
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new login
  # @param [Integer] user_id
  # @param [String] ip_address
  # @param [String] user_agent	
  # @param [String] device_id
  # @param [String] operation_type
  # @return [Integer] id
  # @return [Integer] user_id
  # @return [String] ip_address
  # @return [String] user_agent	
  # @return [String] device_id
  # @return [String] operation_type	
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @login = Login.new(login_params)

    respond_to do |format|
      if @login.save
        format.html { redirect_to @login, notice: 'Login was successfully created.' }
        format.json { render :show, status: :created, location: @login }
      else
        format.html { render :new }
        format.json { render json: @login.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /logins/1
  # PATCH/PUT /logins/1.json
  def update
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
     respond_to do |format|
       if @login.update(login_params)
         format.html { redirect_to @login, notice: 'Login was successfully updated.' }
         format.json { render :show, status: :ok, location: @login }
       else
         format.html { render :edit }
         format.json { render json: @login.errors, status: :unprocessable_entity }
       end
     end
    end
  end

  # destroy login
  # @param [Integer] id
  
  def destroy
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
      @login.destroy
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      sign_out
      respond_to do |format|
        format.html { redirect_to logins_url, notice: 'Login was successfully destroyed.' }
        format.json { head :no_content }
      end
    end      
  end

  def error_page
    render :layout => false
  end

  def lock_page
    render :layout => false
  end

  def check_to_unlock
    @user = User.authenticate(current_user.email,params[:unlock][:password])
    if !(@user)  
      @user_sessions = Login.where(user_id: current_user.id).all
        if (@user_sessions  != nil)
          @user_sessions.all.each do |user_sessions|
              user_sessions.destroy
          end
        end
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      session[:refered_by] = nil
      session[:last_visit] = nil
      sign_out
      redirect_to sign_in_path 
         #redirect_to lock_page_path , notice: "wrong password"   
    else
      session[:last_visit] = Time.now
      redirect_to root_path , notice: "welcome again!"   
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login
      @login = Login.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def login_params
      params.require(:login).permit(:user_id, :ip_address, :user_agent, :device_id)
    end
end
