class CardsController < ApplicationController
  before_action :set_card, only: [:show, :edit, :update, :destroy]

  # GET list of Cards and display it
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index()
    @cards = Card.all
  end

  # GET a spacific card and display it
  # @param [Integer] id card unique ID.
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show()
  end

  # GET a new Card params
  # @param [Integer] id card unique ID (Created automatically).
  # @param [Integer] number card unique number (Created automatically and must be 16 digit).
  # @param [Integer] value card value.
  # @param [Integer] status card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @param [Date] expired_at card expired Date ( Must be equal or greater than 2 days from now ).
  # @param [Integer] invoice_id card invoice ID.
  def new
    @card = Card.new
  end

  # POST a new Card and save it
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @card = Card.new(card_params)
    @user = params[:card][:user_id]
    @card.user_id = User.where("username LIKE ? ", "%#{@user}%").pluck(:id).first

    respond_to do |format|
      if @card.save
        CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
        format.html { redirect_to @card, notice: 'Card was successfully created.' }
        format.json { render json: @card, status: :created, location: @card }
      else
        format.html { render :new }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET an existing card to edit params
  # @param [Integer] id card unique ID.
  # @param [Integer] value card value.
  # @param [Integer] status card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @param [Date] expired_at card expired Date ( Must be equal or greater than 2 days from now ).
  def edit
  end

  # Change an existing card params(value,status,expired_at,user_id)
  # @return [id] card unique ID.
  # @return [number] card unique number (unique number can not be changed).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @card.update(card_params)

        if params[:card][:status].to_i == 2
          CardsLog.create(:user_id => current_user.id.to_i,:action_type => 2,:ip => request.remote_ip,:card_id => @card.id)
        end

        format.html { redirect_to @card, notice: 'Card was successfully updated.' }
        format.json { render :show, status: :ok, location: @card }
      else
        format.html { render :edit }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # Create a new card with a spacific Category value
  # @param [Integer]  value card value. (Created automatically from Card Category value).
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def buy_card

    @new_card =  Card.create(:value => params[:val],:expired_at => Date.today + 3.days,:status => 1,:user_id => 1)
    @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
    
    if @new_card
      redirect_to(display_cards_categories_path,:notice => 'Card was successfully created')
    end
    

  end

  # Change Card status from enabled to suspend
  # @param [Integer] id card unique ID (Created automatically).
  # @param [Integer] status card status (1 for Active, 4 for Disabled).
  # @return [number] card unique number.
  # @return [value] card value.
  # @return [status] card status (1 for Active, 4 for Disabled).
  # @return [expired_at] card expired Date.
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def cards_status

    @card_status = Card.where(:id => params[:id].to_i).first
    if @card_status.status ==  4
      @card_status.update(:status => 1)
      redirect_to(cards_path,:notice => 'Card was successfully enabled')
    else
      @card_status.update(:status => 4)
      redirect_to(cards_path,:notice => 'Card was successfully disabled')
    end

  end

  # GET a spacific Card date and search for any existing card
  # @param [String] search search word that you want to find (number OR value).
  # @param [Date] searchdatefrom search expiration date from.
  # @param [Date] searchdateto search expiration date to.
  # @param [Integer] status search status than you want.
  # @param [Integer] search_user search user ID than you want.
  # @return [id] card unique ID
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date.
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def search

    @search = params[:search].delete('-')
    @searchdatefrom = params[:searchdatefrom]
    @searchdateto = params[:searchdateto]
    @status = params[:status]
    @search_user = params[:search_user]

    @Cards = Card.joins("INNER JOIN users ON users.id = cards.user_id").distinct.all
    @Cards = @Cards.where("cards.expired_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
    @Cards = @Cards.where("cards.expired_at <= ? ", @searchdateto)   if  @searchdateto.present?
    @Cards = @Cards.where("cards.expired_at <= ? AND cards.expired_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    @Cards = @Cards.where(["cards.number LIKE ? OR cards.value = ?","%#{@search}%",@search]).distinct.all if  @search.present?
    @Cards = @Cards.where("cards.status = ? ", @status)   if  @status.present?
    @Cards = @Cards.where("users.username LIKE ? ", "%#{@search_user}%")   if  @search_user.present?

  end

  def searchpost
  end

  # DELETE an existing card
  # @param [id] card unique ID.
  def destroy
    @card.destroy
    CardsLog.create(:user_id => 1,:action_type => 3,:ip => request.remote_ip,:card_id => @card.id)
    respond_to do |format|
      format.html { redirect_to cards_url, notice: 'Card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card
      @card = Card.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_params
      params.require(:card).permit(:operation_id, :number, :value, :status, :expired_at, :user_id, :invoice_id)
    end
end
