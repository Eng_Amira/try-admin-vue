class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :destroy]

  # GET list of notifications and display it
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @notifications = Notification.all
  end

  # GET a Specific existing notification
  # @param [Integer] id notification unique ID.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET list of notifications log of a specific user
  # @param [Integer] user_id notification user unique ID.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_notifications
    @user_notifications = Notification.where(user_id: 1).all
  end
  
  # GET a spacific notification data and search for any existing notification
  # @param [String] search search word that you want to find (title , description).
  # @param [Date] searchdatefrom search created at date from.
  # @param [Date] searchdateto search created at date to.
  # @param [Integer] notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @param [Integer] search_user search user ID than you want.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def search

    @search = params[:search]
    @searchdatefrom = params[:searchdatefrom]
    @searchdateto = params[:searchdateto]
    @notification_type = params[:notification_type]
    @search_user = params[:search_user]

    @notifications = Notification.joins("INNER JOIN users ON users.id = notifications.user_id").distinct.all
    @notifications = @notifications.where("notifications.created_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
    @notifications = @notifications.where("notifications.created_at <= ? ", @searchdateto)   if  @searchdateto.present?
    @notifications = @notifications.where("notifications.created_at <= ? AND notifications.created_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    @notifications = @notifications.where(["notifications.title LIKE  ? OR notifications.description LIKE  ?","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    @notifications = @notifications.where("notifications.notification_type = ? ", @notification_type)   if  @notification_type.present?
    @notifications = @notifications.where("users.id = ? OR users.username LIKE  ?", @search_user,"%#{@search_user}%")   if  @search_user.present?

  end

  # DELETE a Specific notification
  # @param [Integer] id notification unique ID.
  def destroy
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:user_id, :title, :description, :notification_type)
    end
end
