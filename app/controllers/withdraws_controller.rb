class WithdrawsController < ApplicationController
  before_action :set_withdraw, only: [:show, :edit, :update, :destroy]

  # Get list of all the user withdraw operations
  # @return [id] withdraw unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for accepted , 3 for cancelled).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @withdraws = Withdraw.all
  end

  # GET a spacific withdraw operation and display it
  # @param [Integer] id withdraw unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for accepted , 3 for cancelled).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new withdraw operation
  # @param [Integer] id withdraw unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @param [Integer] bank_id the user bank account id.
  # @param [String] operation_id the money operation unique id.
  # @param [Float] amount withdraw amount.
  # @param [Float] fees company fees.
  # @param [Integer] status withdraw status (1 for Pending, 2 for Accepted , 3 for Cancelled).
  # @param [String] withdraw_type withdraw bank type (local_bank, electronic_bank , digital_bank).
  # @param [String] note withdraw user note.
  # @param [Date] created_at Date created.
  # @param [Date] updated_at Date Updated.
  def new
    @withdraw = Withdraw.new
  end

  # GET an existing withdraw and edit params
  # @param [Integer] bank_id the user bank account id.
  # @param [Float] amount withdraw amount.
  # @param [Float] fees company fees.
  # @param [Integer] status withdraw status (1 for Pending, 2 for Accepted , 3 for Cancelled).
  # @param [String] withdraw_type withdraw bank type (local_bank, electronic_bank , digital_bank).
  # @param [String] note withdraw user note.
  def edit
  end

  # POST a new withdraw operation and save it
  # @return [id] withdraw unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for accepted , 3 for cancelled).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @withdraw = Withdraw.new(withdraw_params)
    @message = Withdraw.checkwithdraw(@withdraw.user_id,@withdraw.amount)
    if @message == ""
      ActiveRecord::Base.transaction do
        @user_wallet = UserWallet.where(:user_id => @withdraw.user_id.to_i).first
        @user_balance = @user_wallet.amount
        @user_new_balance = @user_balance.to_f - @withdraw.amount.to_f
        @user_wallet.update(:amount => @user_new_balance)
        @bankfees = CompanyBank.where(:bank_key => @withdraw.bank_account.bank_name.to_s ).first
        @result = @withdraw.amount.to_f * (@bankfees.ratio / 100) + @bankfees.fees
        if @withdraw.status.to_i == 1
          @smstext = "A #{@withdraw.amount.to_f} USD withdraw request was successfully created by payers admin and waiting comfirmation"
          @notification = access_notification(params[:withdraw][:user_id].to_i,@smstext)
          @op_status = 0
        elsif @withdraw.status.to_i == 2
          @smstext = "A #{@withdraw.amount.to_f} USD withdraw request was successfully created and confirmed by payers admin"
          @notification = access_notification(params[:withdraw][:user_id].to_i,@smstext)
          @op_status = 1
        end
        @operation = MoneyOp.create(optype:3 ,amount:@withdraw.amount ,payment_gateway:@withdraw.bank_account.bank_name ,status:@op_status ,user_id:@withdraw.user_id.to_i ,payment_date: Time.now , fees:@result)
      end

      respond_to do |format|
        @withdraw.operation_id = @operation.opid
        @withdraw.fees = @result
        if @withdraw.save
          format.html { redirect_to @withdraw, notice: 'Withdraw was successfully created.' }
          format.json { render :show, status: :created, location: @withdraw }
        else
          format.html { render :new }
          format.json { render json: @withdraw.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(new_withdraw_path,:notice => @message )
    end
  end

  # Change an existing withdraw operation params
  # @return [id] withdraw unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for accepted , 3 for cancelled).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    @status = params[:withdraw][:status].to_i
    @amount = @withdraw.amount
    @operation_status = MoneyOp.where(:opid => @withdraw.operation_id).first
    @old_status = Withdraw.where(id: @withdraw.id ).pluck(:status).first
    if @status == 2 && @old_status.to_i != 2
      @operation_status.update(:status => 1)
      @smstext = "Your #{@amount} USD withdraw request was successfully confirmed by payers admin"
      @notification = access_notification(@withdraw.user_id,@smstext)
    elsif @status == 3 && @old_status.to_i != 3
      @operation_status.update(:status => 0)
      @smstext = "Your #{@amount} USD withdraw request was cancelled by payers admin"
      @notification = access_notification(@withdraw.user_id,@smstext)
    end
    respond_to do |format|
      if @withdraw.update(withdraw_params)
        format.html { redirect_to @withdraw, notice: 'Withdraw was successfully updated.' }
        format.json { render :show, status: :ok, location: @withdraw }
      else
        format.html { render :edit }
        format.json { render json: @withdraw.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET a list of a spacific user bank accounts
  # @param [Integer] user_id user unique ID.
  # @return [Integer] id bank account unique ID.
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [user_id] bank account user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_bank_accounts
    @user_id = params[:user_id].to_i
    @banks = BankAccount.where(:user_id => @user_id ).pluck('bank_name' , 'id')
    render :json => {'userbanks': @banks }
  end

  # DELETE an existing withdraw operation
  # @param [id] withdraw operation unique ID.
  def destroy
    @withdraw.destroy
    respond_to do |format|
      format.html { redirect_to withdraws_url, notice: 'Withdraw was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_withdraw
      @withdraw = Withdraw.find(params[:id])
    end

    def access_notification(user_id,smstext)

      @user_id = user_id
      @smstext = smstext
      @user_notification_setting = NotificationsSetting.where(user_id: @user_id).first
      @user_data = User.where(id: @user_id).first

        @title = "Balance Withdraw"
        Notification.create(user_id: @user_id ,title: @title, description: @smstext , notification_type: @user_notification_setting.money_transactions)
        
      if @user_notification_setting.pending_transactions == 3
        SMSNotification.sms_notification_setting(@user_data.telephone,@smstext)
        SmsLog.create(:user_id => @user_id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:@title,text:@smstext)
      elsif @user_notification_setting.pending_transactions == 2
        SMSNotification.sms_notification_setting(@user_data.telephone,@smstext)
        SmsLog.create(:user_id => @user_id, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.pending_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:@title,text:@smstext)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def withdraw_params
      params.require(:withdraw).permit(:user_id, :bank_id, :operation_id, :amount, :fees, :status, :withdraw_type, :note)
    end
end
