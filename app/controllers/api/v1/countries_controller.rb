class Api::V1::CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!, except: [:index, :show]
  #before_action :require_login
  skip_before_action :verify_authenticity_token
  attr_reader :current_user

      # GET /countries
      # GET /countries.json
      # =begin
      # @api {get} /api/v1/countries List all countries
      # @apiVersion 0.3.0
      # @apiName GetCountries
      # @apiGroup Countries
      # @apiDescription get details of all countries.
      # @apiExample Example usage:
      # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -i http://localhost:3000/api/v1/countries
      #
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiError NoAccessRight Only authenticated users can access the data.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end
  def index    
    @countries = Country.all
    respond_to do |format|   
         format.json { render json: @countries}  
    end
  end

      # GET /countries/1
      # GET /countries/1.json
      # =begin
      # @api {get} /api/countries/{:id} Get Country Data
      # @apiVersion 0.3.0
      # @apiName GetCountry
      # @apiGroup Countries
      # @apiDescription get details of specific country.
      # @apiExample Example usage:
      # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -i http://localhost:3000/api/v1/countries/1
      #
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiError NoAccessRight Only authenticated users can access the data.
      # @apiError CountryNotFound   The <code>id</code> of the Country was not found.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "CountryNotFound"
      #    }
      # =end
  def show
    @country = Country.where("id =? " ,params[:id]).first
      respond_to do |format|
        format.json { render json: @country }
      end
  end

      # POST /countries
      # POST /countries.json
      # =begin
      # @api {post} /countries/ Create new Country
      # @apiVersion 0.3.0
      # @apiName postCountry
      # @apiGroup Countries
      # @apiPermission admin
      # @apiDescription create new Country.
      # @apiParam {String}     short_code             Country Code.
      # @apiParam {String}     Full_Name              Country Name.
      # @apiParam {String}     Phone_code             Country Phone Code.
      # @apiParam {String}     Currency               Country Currency.
      # @apiParam {String}     language               Country Language.
      # @apiParam {Integer}    active                 Country Status.
      #
      # @apiExample Example usage:       
      # curl -X POST  http://localhost:3000/api/v1/countries -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"country": {"short_code": "SAU","Full_Name": "Saudi Arabia	","Phone_code": "00966", "Currency": "SAR", "language": "arabic","active": 1}}' 
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiSuccessExample Response (example):
      #     HTTP/ 200 OK
      #     {
      #       "success": "200 ok"
      #    }
      #
      # @apiError NoAccessRight Only authenticated admin can create Countries.
      #
      # @apiError MissingToken invalid-token.
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 400 Bad Request
      #     {
      #       "error": "Missing Token"
      #    }
      # =end
  def create
    @country = Country.new(country_params)

    respond_to do |format|
      #if current_user.roleid == 1 
        if @country.save
           format.json { render json: @country, status: :created, location: @country }
        else
           format.json { render json: @country.errors, status: :unprocessable_entity }
        end
      #else
       # format.json { render json: {\"errors\":[\"Not Authenticated\"]}, status: :unprocessable_entity }
      #end
    end
  end

      # PATCH/PUT /countries/1
      # PATCH/PUT /countries/1.json
      # =begin
      # @api {put} /countries/:id/edit Update Country 
      # @apiVersion 0.3.0
      # @apiName PutCountry
      # @apiGroup Countries
      # @apiPermission admin
      # @apiDescription update data of a Country.
      #
      # @apiParam {String}     short_code             Country Code.
      # @apiParam {String}     Full_Name              Country Name.
      # @apiParam {String}     Phone_code             Country Phone Code.
      # @apiParam {String}     Currency               Country Currency.
      # @apiParam {String}     language               Country Language.
      # @apiParam {Integer}    active                 Country Status.
      #
      # @apiExample Example usage:
      # curl -X PUT http://localhost:3000/api/v1/countries/1 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ "short_code": "test edit"}'
      #
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiError NoAccessRight Only authenticated admins can update the data.
      # @apiError CountryNotFound   The <code>id</code> of the Country was not found.
      #
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "CountryNotFound"
      #    }
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Not Authenticated
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end
  def update
    respond_to do |format|
     # if current_user.roleid == 1 
         if @country.update(country_params)
            format.json { render json: @country, status: :ok, location: @country }
         else
            format.json { render json: @country.errors, status: :unprocessable_entity }
         end
     # else
       #   format.json { render json: "notallowed", status: :notallowed }
     # end
    end
  end

      # DELETE /countries/1
      # DELETE /countries/1.json
      # =begin
      # @api {delete} /countries/:id Delete Country 
      # @apiVersion 0.3.0
      # @apiName DeleteCountry
      # @apiGroup Countries
      # @apiPermission admin
      # @apiDescription Delete a Country.
      #
      # @apiExample Example usage:
      # curl -X DELETE http://localhost:3000/api/v1/countries/2 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4"
      #
      # @apiSuccessExample Response (example):
      #     HTTP/ 200 Success
      #     {
      #       "success": "200 ok"
      #    }
      # @apiError NoAccessRight Only  authenticated admins can delete the country.
      # @apiError CountryNotFound   The <code>id</code> of the Country was not found.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Not Authenticated
      #     {
      #       "error": "NoAccessRight"
      #    }
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "CountryNotFound"
      #    }
      # =end

  def destroy    
    respond_to do |format|
      # if current_user.roleid == 1 
        if @country.destroy
            format.json { head :no_content }
        end
      #else
       # format.json { render json: "notallowed", status: :notallowed }
      #end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:short_code, :Full_Name, :Phone_code, :Currency, :language, :active)
    end
end
