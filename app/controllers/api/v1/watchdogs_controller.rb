module Api
  module V1
   class WatchdogsController < ApplicationController
     #before_action :require_login 
     before_action :set_watchdog, only: [:show, :edit, :update, :destroy]
     skip_before_action :verify_authenticity_token
  # GET /watchdogs
  # GET /watchdogs.json
  def index
   # if current_user.roleid == 1
   respond_to do |format|
      @watchdogs = Watchdog.all
      format.json { render json: @watchdogs } 
    #  else
       # redirect_to root_path , notice: "not allowed" 
   end  
   # end    
  end

  # GET /watchdogs/1
  # GET /watchdogs/1.json
  def show
    if current_user.roleid != 1 and current_user.id != params[:id].to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

      # show user log
      # GET /user_log
      #=begin
      # @api {get} /api/v1/user_log?id={:user-id} show user log
      # @apiVersion 0.3.0
      # @apiName Getuserlog
      # @apiGroup Log
      # @apiDescription show user log.
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/user_log?id=1
      #
      # @apiSuccess {Integer}     id                    Users-ID.
      # @apiSuccess {Integer}     ipaddress             Ip address
      # @apiSuccess {String}      operation_type        Details	.
      # @apiSuccess {Datetime}    created_at            Date of creating the Log.
      # @apiSuccess {Datetime}    updated_at            Date of updating the Log.
      #
      # @apiError NoAccessRight Only authenticated users can perform this action.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      #=end 

  def user_log
    #if current_user.roleid == 1 or current_user.id == params[:id].to_i
      @log = Watchdog.where("user_id =? ",params[:id]).joins("INNER JOIN users ON users.id = watchdogs.user_id").distinct.all.order('created_at DESC')
      respond_to do |format|
        format.json { render json: @log }
      end 
      #else
      #redirect_to root_path , notice: "not allowed" 
    # end
  end

  # GET /watchdogs/new
  def new
    #if current_user.roleid == 1 
      @watchdog = Watchdog.new
    #else
     # redirect_to root_path , notice: "not allowed" 
    #end
  end

  # GET /watchdogs/1/edit
  def edit
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # POST /watchdogs
  # POST /watchdogs.json
  
  # curl -d '{"user_id": 1,"logintime": "2018-07-12 12:08:00"}' -H 'cache-control: no-cache' -H 'content-type: application/json'  -X POST  http://localhost:3000/api/v1/watchdogs
  def create
    @watchdog = Watchdog.new(watchdog_params)

    respond_to do |format|
      if @watchdog.save
        format.json { render json: @watchdog, status: :created, location: @watchdog }
      else
        format.json { render json: @watchdog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /watchdogs/1
  # PATCH/PUT /watchdogs/1.json
  def update
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
      respond_to do |format|
        if @watchdog.update(watchdog_params)
          format.html { redirect_to @watchdog, notice: 'Watchdog was successfully updated.' }
          format.json { render :show, status: :ok, location: @watchdog }
        else
          format.html { render :edit }
          format.json { render json: @watchdog.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /watchdogs/1
  # DELETE /watchdogs/1.json
  def destroy
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
       @watchdog.destroy
       respond_to do |format|
         format.html { redirect_to watchdogs_url, notice: 'Watchdog was successfully destroyed.' }
         format.json { head :no_content }
       end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watchdog
      @watchdog = Watchdog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watchdog_params
      params.require(:watchdog).permit(:user_id, :logintime, :ipaddress, :lastvisit)
    end
end
end
end