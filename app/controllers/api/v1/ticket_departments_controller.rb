module Api
    class TicketDepartmentsController < ApplicationController
        before_action :set_ticket_department, only: [:show, :edit, :update, :destroy]
        skip_before_action :verify_authenticity_token

        # GET /ticket_departments
        # GET /ticket_departments.json
        def index
            # =begin
    # @api {get} /api/ticket_departments 1-Request ticket departments List
    # @apiVersion 0.3.0
    # @apiName GetTicketDepartments
    # @apiGroup Ticket departments
    # @apiExample Example usage:
    # curl -i http://localhost:3000/api/ticket_departments
    # @apiSuccess {Number} id department unique ID.
    # @apiSuccess {string} name department name.
    # @apiSuccess {string} code department code.
    # @apiSuccess {string} description department description.
    # @apiSuccess {Date} created_at  Date created.
    # @apiSuccess {Date} updated_at  Date Updated.
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # [
    #      {
    #          "id": 1,
    #          "name": "department 1",
    #          "code": "AS",
    #          "description": "Description Description",
    #          "created_at": "2018-09-08T12:22:09.000Z",
    #          "updated_at": "2018-09-08T12:22:09.000Z"
    #      },
    #      {
    #          "id": 2,
    #          "name": "department 2",
    #          "code": "AW",
    #          "description": "Description Description",
    #          "created_at": "2018-09-08T14:20:07.000Z",
    #          "updated_at": "2018-09-08T14:20:07.000Z"
    #      }
    # ]
    # @apiError MissingToken invalid Token.
    # @apiErrorExample Error-Response:
    # HTTP/1.1 400 Bad Request
    #   {
    #     "error": "Missing token"
    #   }
    # =end
    @ticket_departments = TicketDepartment.all
    respond_to do |format|
        format.json { render json: @ticket_departments }
    end
        end
    
        # GET /ticket_departments/1
        # GET /ticket_departments/1.json
        def show
        # =begin
        # @api {get} /api/ticket_departments/{:id} 3-Request Specific Ticket department
        # @apiVersion 0.3.0
        # @apiName GetSpecificTicketDepartments
        # @apiGroup Ticket departments
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/ticket_departments/1
        # @apiParam {Number} id department ID.
        # @apiSuccess {Number} id department unique ID.
        # @apiSuccess {string} name department name.
        # @apiSuccess {string} code department code.
        # @apiSuccess {string} description department description.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #       "id": 1,
        #       "name": "department 1",
        #       "code": "AS",
        #       "description": "Description Description",
        #       "created_at": "2018-09-08T12:22:09.000Z",
        #       "updated_at": "2018-09-08T12:22:09.000Z"
        #      }
        # @apiError TicketDepartmentNotFound The id of the Ticket department was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TicketDepartmentNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
            format.json { render json: @ticket_department }
          end
        end
    
        # GET /ticket_departments/new
        def new
        @ticket_department = TicketDepartment.new
        end
    
        # GET /ticket_departments/1/edit
        def edit
        end
    
        # POST /ticket_departments
        # POST /ticket_departments.json
        def create
            # =begin
        # @api {post} /api/ticket_departments 2-Create a new ticket department
        # @apiVersion 0.3.0
        # @apiName PostTicketDepartments
        # @apiGroup Ticket departments
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/ticket_departments \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "name": "department 3",
        #   "code": "AK",
        #   "description": "Description 3"
        # }'
        # @apiParam {string} name department name.
        # @apiParam {string} code department code.
        # @apiParam {string} description department description.

        
        # @apiSuccess {Number} id department unique ID.
        # @apiSuccess {string} name department name.
        # @apiSuccess {string} code department code.
        # @apiSuccess {string} description department description.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 3,
        #         "name": "department 3",
        #         "code": "AK",
        #         "description": "Description 3",
        #         "created_at": "2018-09-08T14:25:02.000Z",
        #         "updated_at": "2018-09-08T14:25:02.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @ticket_department = TicketDepartment.new(ticket_department_params)
    
        respond_to do |format|
            if @ticket_department.save
                format.json { render json: @ticket_department, status: :created, location: @ticket_department }
            else
            format.json { render json: @ticket_department.errors, status: :unprocessable_entity }
            end
        end
        end
    
        # PATCH/PUT /ticket_departments/1
        # PATCH/PUT /ticket_departments/1.json
        def update
            # =begin
        # @api {put} /api/ticket_departments/{:id} 4-Update an existing ticket department
        # @apiVersion 0.3.0
        # @apiName PutTicketDepartments
        # @apiGroup Ticket departments
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/ticket_departments/6 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "code": "AT"
        # }'
        # @apiParam {string} code department code.

        # @apiSuccess {Number} id department unique ID.
        # @apiSuccess {string} name department name.
        # @apiSuccess {string} code department code.
        # @apiSuccess {string} description department description.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 3,
        #         "code": "AT",
        #         "name": "department 3",
        #         "description": "Description 3",
        #         "created_at": "2018-09-08T14:25:02.000Z",
        #         "updated_at": "2018-09-08T14:29:29.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError TicketDepartmentNotFound The id of the Ticket department was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TicketDepartmentNotFound"
        #   }
        # =end


        respond_to do |format|
            if @ticket_department.update(ticket_department_params)
            format.json { render json: @ticket_department, status: :ok, location: @ticket_department }
            else
            format.json { render json: @ticket_department.errors, status: :unprocessable_entity }
            end
        end
        end
    
        # DELETE /ticket_departments/1
        # DELETE /ticket_departments/1.json
        def destroy
        @ticket_department.destroy
        respond_to do |format|
            format.html { redirect_to ticket_departments_url, notice: 'Ticket department was successfully destroyed.' }
            format.json { head :no_content }
        end
        end
    
        private
        # Use callbacks to share common setup or constraints between actions.
        def set_ticket_department
            @ticket_department = TicketDepartment.find(params[:id])
        end
    
        # Never trust parameters from the scary internet, only allow the white list through.
        def ticket_department_params
            params.require(:ticket_department).permit(:name, :code, :description)
        end
    end
end