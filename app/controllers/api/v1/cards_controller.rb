module Api
  module V1
    class CardsController < ApplicationController
       before_action :set_card, only: [:show, :edit, :update, :destroy]
       skip_before_action :verify_authenticity_token

      # GET /cards
      def index
        @cards = Card.all
        respond_to do |format|
          format.json { render json: @cards }
        end

      # =begin
      # @api {get} /api/v1/cards 1-Request cards List
      # @apiVersion 0.3.0
      # @apiName GetCard
      # @apiGroup Cards
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/cards
      # @apiSuccess {Number} id card unique ID.
      # @apiSuccess {Number} number card unique number.
      # @apiSuccess {Number} value card value.
      # @apiSuccess {Number} status card status.
      # @apiSuccess {Date} expired_at  card expired Date.
      # @apiSuccess {Number} invoice_id card invoice ID.
      # @apiSuccess {Date} created_at  Date created.
      # @apiSuccess {Date} updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # [
      #      {
      #         "id": 5,
      #         "number": 1234567891234566,
      #         "value": 150,
      #         "status": 3,
      #         "expired_at": "2018-07-24",
      #         "user_id": 2,
      #         "invoice_id": 0,
      #         "created_at": "2018-07-24T12:50:56.749Z",
      #         "updated_at": "2018-07-24T12:51:18.567Z"
      #     },
      #     {
      #         "id": 6,
      #         "number": 5722704937640978,
      #         "value": 200,
      #         "status": 1,
      #         "expired_at": "2022-06-25",
      #         "user_id": 1,
      #         "invoice_id": 0,
      #         "created_at": "2018-07-24T13:24:23.665Z",
      #         "updated_at": "2018-07-24T13:24:23.665Z"
      #     }
      # ]
      # @apiError MissingToken invalid Token.
      # @apiErrorExample Error-Response:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      end

      # GET /cards/:id
      def show
        # =begin
        # @api {get} /api/v1/cards/{:id} 2-Request Specific Card
        # @apiVersion 0.3.0
        # @apiName GetSpecificCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards/1
        # @apiParam {Number} id Card ID.
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 1,
        #         "number": 5722702457640936,
        #         "value": 100,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-24T13:24:23.665Z",
        #         "updated_at": "2018-07-24T13:24:23.665Z"
        #     }
        # @apiError CardNotFound The id of the Card was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CardNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
          format.json { render json: @card }
        end

      end

      # GET /cards/new
      def new
        @card = Card.new
      end

      # GET /cards/:id/edit
      def edit
      end

      # POST /cards
      def create

        # =begin
        # @api {post} /api/v1/cards 3-Create a new Card
        # @apiVersion 0.3.0
        # @apiName PostCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/cards \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "number": 1111111122222222,
        #   "value": 300,
        #   "status": 1,
        #   "expired_at": "2018-07-26",
        #   "user_id": 3,
        #   "invoice_id": 0
        # }'
        # @apiParam {Number} number card number must be 16 digit.
        # @apiParam {Number} value card value.
        # @apiParam {Number} status card status.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Number} user_id card user ID.
        # @apiParam {Number} invoice_id card invoice ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 7,
        #         "number": 1178702457640936,
        #         "value": 500,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-25T13:24:23.665Z",
        #         "updated_at": "2018-07-25T13:24:23.665Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError ExistingNumber card number has already been taken.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Existing Number
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError InvalidNumber card number not equal 16 digit.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Number
        #   {
        #     "error": "must be 16 digit long"
        #   }
        # =end

        @card = Card.new(card_params)
        respond_to do |format|
          if @card.save
            CardsLog.create(:user_id => 1,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
            format.json { render json: @card, status: :created, location: @card }
          else
            format.json { render json: @card.errors, status: :unprocessable_entity }
          end
        end

      end

      # PATCH/PUT /cards/:id
      def update

        # =begin
        # @api {put} /api/v1/cards/{:id} 4-Update an existing Card
        # @apiVersion 0.3.0
        # @apiName PutCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/cards/6 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "value": 400,
        #   "status": 2,
        #   "expired_at": "2018-07-26",
        #   "user_id": 3
        # }'
        # @apiParam {Number} value card value.
        # @apiParam {Number} status card status.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Number} user_id card user ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 6,
        #         "number": 1155463457640936,
        #         "value": 200,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-25T13:24:23.665Z",
        #         "updated_at": "2018-07-25T13:24:23.665Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError CardNotFound The id of the Card was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CardNotFound"
        #   }
        # =end

        respond_to do |format|
          if @card.update(card_params)

            if params[:card][:status].to_i == 2
              CardsLog.create(:user_id => 1,:action_type => 2,:ip => request.remote_ip,:card_id => @card.id)
            end
            format.json { render json: @card, status: :ok, location: @card }
          else
            format.json { render json: @card.errors, status: :unprocessable_entity }
          end
        end

      end

      def cards_status

        # =begin
        # @api {get} /api/v1/cards_status?id={:id} 6-Suspend a Specific Card
        # @apiVersion 0.3.0
        # @apiName SuspendCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_status?id=22
        # @apiParam {Number} id card unique ID.
        # @apiSuccess {Number} status card status(1 for enable & 4 for suspend).
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "status": 1,
        #   "id": 22,
        #   "number": 8250409147380401,
        #   "value": 200,
        #   "expired_at": "2018-07-31",
        #   "user_id": 1,
        #   "invoice_id": 0,
        #   "created_at": "2018-07-28T10:07:53.960Z",
        #   "updated_at": "2018-07-29T08:29:33.484Z"
        # }
        # @apiError CardNotFound The ID of this Card was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Card Not Found"
        #   }
        # =end

        @card_status = Card.where(:id => params[:id].to_i).first
        flash[:error]= "Card Not Found"
        respond_to do |format|
          if @card_status

            if @card_status.status ==  4
              if @card_status.update(:status => 1)
                format.json { render json: @card_status }
              end
            else
              if @card_status.update(:status => 4)
                format.json { render json: @card_status }
              end
            end

          else
            format.json { render json: flash }
          end

        end
    
      end

      def search

        # =begin 
        # @api {get} /api/v1/search?&search={:search_word}&search_user={:user_id}&status={:status}&searchdatefrom={:expired_at_from}&searchdateto={:expired_at_to} 5-Search for an existing Card
        # @apiVersion 0.3.0
        # @apiName GetCardSearch
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/search?=&search=100&search_user=1&status=1&searchdatefrom=2018-07-24&searchdateto=2018-07-25
        # @apiParam {Number} number card number.
        # @apiParam {Number} value card value.
        # @apiParam {Number} search_word card search number or value.
        # @apiParam {Number} status card status(Active ,Charged ,pending ,disabled).
        # @apiParam {Number} user_id card User ID.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Date} expired_at_from  card search expired Date from.
        # @apiParam {Date} expired_at_to  card search expired Date to.
        # @apiParam {Number} invoice_id card invoice ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 2,
        #   "number": 1234567891234567,
        #   "value": 120,
        #   "status": 1,
        #   "expired_at": "2018-07-25",
        #   "user_id": 1,
        #   "invoice_id": 0,
        #   "created_at": "2018-07-24T12:43:16.492Z",
        #   "updated_at": "2018-07-25T14:01:18.051Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError NoResults There are no results for your search. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 No Results
        #   {
        #     "error": "NoResults"
        #   }
        # =end

        @search = params[:search].delete('-')
        @searchdatefrom = params[:searchdatefrom]
        @searchdateto = params[:searchdateto]
        @status = params[:status]
        @search_user = params[:search_user]
    
        @Cards = Card.all
        @Cards = @Cards.where("cards.expired_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
        @Cards = @Cards.where("cards.expired_at <= ? ", @searchdateto)   if  @searchdateto.present?
        @Cards = @Cards.where("cards.expired_at <= ? AND cards.expired_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
        @Cards = @Cards.where(["number LIKE  ? OR value = ?","%#{@search}%",@search]).distinct.all if  @search.present?
        @Cards = @Cards.where("cards.status = ? ", @status)   if  @status.present?
        @Cards = @Cards.where("cards.user_id = ? ", @search_user)   if  @search_user.present?

        respond_to do |format|
            format.json { render json: @Cards }
        end
    
      end
    
      def searchpost
      end

      def buy_card

        # =begin
        # @api {get} /api/v1/buy_card?val={:value} 7-Buy a Card
        # @apiVersion 0.3.0
        # @apiName BuyCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/buy_card?val=1000
        # @apiParam {Number} value card value.
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status(1 for active).
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 23,
        #   "number": 1650927974431984,
        #   "value": 1000,
        #   "status": 1,
        #   "expired_at": "2018-07-31",
        #   "user_id": 2,
        #   "invoice_id": 0,
        #   "created_at": "2018-07-28T15:06:25.684Z",
        #   "updated_at": "2018-07-29T08:41:02.510Z"
        # }
        # @apiError CardWasNotCreated something went wrong while saving this card. 
        # @apiErrorExample Error-Response:
        #   {
        #     "error": "Card Was Not Created"
        #   }
        # =end

        @new_card =  Card.create(:value => params[:val],:expired_at => Date.today + 3.days,:status => 1,:user_id => 1)
        flash[:error] = "Card Was Not Created"
        
        respond_to do |format|
          if @new_card
            format.json { render json: @new_card }
          else
            format.json { render json: flash }      
          end

        end
        
    
      end

      # DELETE /cards/:id
      def destroy
        @card.destroy
        CardsLog.create(:user_id => 1,:action_type => 3,:ip => request.remote_ip,:card_id => @card.id)
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_card
          @card = Card.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def card_params
          params.require(:card).permit(:number, :value, :status, :expired_at, :user_id, :invoice_id)
        end

    end

  end

end
