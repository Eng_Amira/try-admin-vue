class UserMailer < ApplicationMailer
    default from: "Payers <no-reply@payers.net>"

    # send a mail in case of signing in from different ip     
    def differentip(user,ip)
       
       @email = user.email
       @ip = ip   
             
         mail to: user.email, subject: 'Welcome to My Awesome Site'
    end

    # send a mail to confirm account after registeration
    def confirmmail(user,token)
       
      @email = user.email
      @token = token   
      @id = user.id
            
        mail to: user.email, subject: 'Please Confirm your account'
    end

    # send a mail to confirm sign in
    def confirmsignin(user,token)
       
        @email = user.email
        @token = token   
        @id = user.id
              
          mail to: user.email, subject: 'Please Confirm your account'
    end


    # send a mail to reenable the locked account
    def unlockaccount(user,token)
       
        @email = user.email
        @token = token   
        @id = user.id
              
          mail to: user.email, subject: 'Your account has been locked'
    end

    # send a mail to invite people under user's invitation code
    def invitation(email,user)
         @invitation_code = user.invitation_code
         @invited_by = user.username
          mail to:email, subject: 'Invitation to payers'
    end


    # send a mail after sign in 
    def signin_details(user,ip,agent,device)
        @user = user
        @ip = ip
        @agent = agent
        @device = device
        mail to:@user.email, subject: 'A successful login to your account '
    end

      
end
