json.extract! nationalid_verification, :id, :user_id, :legal_name, :national_id, :type, :issue_date, :expire_date, :status, :note, :created_at, :updated_at
json.url nationalid_verification_url(nationalid_verification, format: :json)
