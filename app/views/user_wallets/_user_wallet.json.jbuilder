json.extract! user_wallet, :id, :currency, :amount, :user_id, :status, :uuid, :created_at, :updated_at
json.url user_wallet_url(user_wallet, format: :json)
