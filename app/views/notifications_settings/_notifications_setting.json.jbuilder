json.extract! notifications_setting, :id, :user_id, :money_transactions, :pending_transactions, :transactions_updates, :help_tickets_updates, :tickets_replies, :account_login, :change_password, :verifications_setting, :created_at, :updated_at
json.url notifications_setting_url(notifications_setting, format: :json)
