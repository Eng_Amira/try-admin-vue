json.extract! selfie_verification, :id, :user_id, :note, :created_at, :updated_at
json.url selfie_verification_url(selfie_verification, format: :json)
