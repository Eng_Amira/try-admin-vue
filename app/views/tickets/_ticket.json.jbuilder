json.extract! ticket, :id, :number, :title, :content, :attachment, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
