json.extract! admin_login, :id, :admin_id, :ip_address, :user_agent, :device_id, :operation_type, :created_at, :updated_at
json.url admin_login_url(admin_login, format: :json)
