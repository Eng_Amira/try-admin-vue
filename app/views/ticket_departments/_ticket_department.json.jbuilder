json.extract! ticket_department, :id, :name, :code, :description, :created_at, :updated_at
json.url ticket_department_url(ticket_department, format: :json)
