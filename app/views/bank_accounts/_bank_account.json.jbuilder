json.extract! bank_account, :id, :bank_name, :branche_name, :swift_code, :country, :account_name, :account_number, :created_at, :updated_at
json.url bank_account_url(bank_account, format: :json)
